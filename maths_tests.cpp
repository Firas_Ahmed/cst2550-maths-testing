#include "catch.hpp"
#include "maths.h"

TEST_CASE("test primes", "[is_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);
  REQUIRE(is_prime(6) == false);
  REQUIRE(is_prime(7) == true);
  REQUIRE(is_prime(8) == false);
  REQUIRE(is_prime(9) == false);

  REQUIRE(absolute(3.69) == 3.69);
  REQUIRE(absolute(3.69) == 3.69);

  REQUIRE(power(5,3) == 125);
  REQUIRE(power(5,-2) == 0.04);
  REQUIRE(power(2.5,2) == 6.25);
  REQUIRE(power(5,0) == 1);
}
