#include "maths.h"

double absolute(double number)
{
  if (number <= -1){
    number = -number;
    return -number;
  } else{
    return number;
  }
}

double power(double number, int exponent)
{
  double result = 1;
  bool negative = exponent < 0;
  if (exponent < 0) exponent = -exponent;
  for(int i = 0; i < exponent; i++) result *= number;
  return negative ? 1 / result : result;
}

bool is_prime(int number)
{
  for (int i = 2; i < number; ++i)
    if (number % i == 0)
      return false;
  return true;
}

