CXX=g++
CFLAGS=-Wall

test: test_main.o maths.o
	$(CXX) maths.o test_main.o maths_tests.cpp $(CFLAGS) -o maths_tests
	./maths_tests

maths.o: maths.cpp
	$(CXX) maths.cpp $(CFLAGS) -c -o maths.o

test_main.o: test_main.cpp
	$(CXX) test_main.cpp $(CFLAGS) -c -o test_main.o

clean:
	rm -f *.o sort_test

.PHONY: clean test